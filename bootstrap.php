<?php
    require_once 'core/Model.php';
    require_once 'core/View.php';
    require_once 'core/Controller.php';
    require_once 'core/Route.php';
    require_once 'core/Database.php';

    spl_autoload_register(function ($class) {
        include $class . '.php';
    });

    Route::start();