<?php

namespace contracts;

interface Logger
{
    public function log ($data);
}