<html>
<head>
    <script
        src="https://code.jquery.com/jquery-3.5.1.min.js"
        integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/static/css/main.css">
    <script src="/static/js/main.js"></script>
</head>
<body>
<div>
    <span id="message">
        <?php
        if ($message) {
            echo $message;
        }
        ?>
    </span>
</div>

<div class="container">
    <div id="form-container">
        <form action="/" id="form" name="form" method="POST">
            <textarea name="dates" id="dates" placeholder="YYYY/mm/dd-mm.dd.YYYY"><?php
                if ($data) echo $data;
                ?></textarea>
        </form>
    </div>
    <div id="buttons">
        <button id="btn-post">
            POST
        </button>
        <button id="btn-ajax">
            AJAX
        </button>
    </div>
</div>

</body>

</html>
