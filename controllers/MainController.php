<?php

use services\DBLogger;
use services\StringService;

class MainController extends Controller
{
    public function action_index()
    {
        if (!empty($_POST)) {
            $dates = $_POST['dates'];
            $result = $this->getResult($dates);
            $this->view->generate('main.php', $dates, $result);
        } else {
            $this->view->generate('main.php');
        }
    }

    public function action_ajax()
    {
        $dates = $_POST['dates'];
        $result = $this->getResult($dates);
        echo $result;
    }

    /**
     * @param $dates
     * @return string
     */
    private function getResult($dates)
    {
        $logger = new DBLogger();
        $result = (new StringService($dates, $logger))->getDaysDiffResult();
        return $result;
    }
}