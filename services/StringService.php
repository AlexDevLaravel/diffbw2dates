<?php

namespace services;
use contracts\Logger;

class StringService
{
    private $str;
    private $logger;
    const SECONDS_IN_DAY = 86400;
    public function __construct($str, Logger $logger)
    {
        $this->str = $str;
        $this->logger = $logger;
    }

    public function getDaysDiffResult()
    {
        $validate = (new DataStringValidateService())->validate($this->str);
        if (!$validate) {
            $result = 'Invalid date format';
        } else {
            $dates = explode('-', $this->str);
            $days = abs((strtotime($dates[0]) - (strtotime($dates[1]))) / self::SECONDS_IN_DAY);
            $result = 'Result: ' . $this->str . ' = ' . $days;
            $data = [
                'user_ip' => $_SERVER['REMOTE_ADDR'],
                'date_from' => $dates[0],
                'date_to' => $dates[1],
                'result' => $days,
                'time' => microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'],
            ];
            $this->logger->log($data);
        }




        return $result;

    }


}