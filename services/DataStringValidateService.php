<?php

namespace services;

use DateTime;

class DataStringValidateService
{
    public function validate($str)
    {
        $result = false;
        $dates = explode('-', $str);
        if (count($dates) == 2) {
            if (strtotime($dates[1]) && strtotime($dates[0])) {
                $result = true;
            } else {
                $result = false;
            }
        } else {
            $result = false;
        }

        return $result;
    }

}