<?php

namespace services;

use contracts\Logger;
use models\DBLogs;

class DBLogger implements Logger
{
    public function log ($data)
    {
        $logger = new DBLogs($data);
        $logger->save();
    }
}