function sendAjax(dates) {
    $.ajax({
        type: "POST",
        url: '/main/ajax',
        data: {
            dates : dates,
        },
        success: function (data) {
            console.log(data);
            $('#message').html(data);
        }
    });
}

$(document).ready(function() {
    var empty_error = 'The dates fiels must not be empty';
    form = $('#form')
    $(document).on('click', '#btn-post', function () {
        if (validateStr($('#dates').val())) {
            form.submit();
        } else {
            $('#message').html(empty_error);
        }
    })
    $(document).on('click', '#btn-ajax', function () {
        if (validateStr($('#dates').val())) {
            sendAjax($('#dates').val());
        } else {
            $('#message').html(empty_error);
        }

    })
});
function validateStr(str) {
    return str.trim();
}