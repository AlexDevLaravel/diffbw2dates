<?php

class Database
{
    public static function getConnection()
    {
        $dbconfig = require 'dbconfig.php';
        $dbn = new PDO($dbconfig['dbq'], $dbconfig['user'], $dbconfig['password'], $dbconfig['opt']);
        $dbn->exec("set names utf8");
        return $dbn;
    }
}
