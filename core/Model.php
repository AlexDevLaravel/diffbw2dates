<?php

namespace core;

interface Model {
    public function save();
}