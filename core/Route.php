<?php

class Route
{
    public static function start()
    {
        $controller_name = 'Main';
        $action_name = 'index';
        $ps = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

        $routes = explode('/', $ps);
        if (!empty($routes[1])) {
            $controller_name = $routes[1];
        }
        if (!empty($routes[2])) {
            $action_name = $routes[2];
        }

        $controller_name = ucfirst($controller_name).'Controller';
        $controller_file = $controller_name.'.php';
        $controller_path = 'controllers/'.$controller_file;
        if (file_exists($controller_path)) {
            include 'controllers/'.$controller_file;
        } else {
            echo "Controller error <br>";
            die();
        }

        $controller = new $controller_name;
        $action = 'action_' . $action_name;

        //check if exists $action metod in $controller instance
        if (method_exists($controller, $action)) {
            $controller->$action();
        } else {
            echo "Controller metod error<br>";
            die();
        }

    }
}