<?php

class View
{
    public function generate($content_view, $data = null, $message = null)
    {
        include 'views/'.$content_view;
    }
}