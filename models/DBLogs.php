<?php

namespace models;

use core\Model;
use Database;
use PDOException;

class DBLogs implements Model
{
    protected $db;
    protected $table;
    protected $data;

    public function __construct($data) {
        $this->db = Database::getConnection();
        $this->data = $data;
        $this->table = 'logs';

    }

    public function save() {
        $arraySetFields = [];
        $arrayData = [];
        foreach($this->data as $field=>$value){
            $arraySetFields[] = $field;
            $arrayData[] = $value;
        }
        $forQueryFields =  implode(', ', $arraySetFields);
        $rangePlace = array_fill(0, count($arraySetFields), '?');
        $forQueryPlace = implode(', ', $rangePlace);

        try {
            $db = $this->db;
            $stmt = $db->prepare("INSERT INTO $this->table ($forQueryFields) values ($forQueryPlace)");
            return $stmt->execute($arrayData);
        }catch(PDOException $e){
            echo 'Error : '.$e->getMessage();
            echo '<br/>Error sql : ' . "'INSERT INTO $this->table ($forQueryFields) values ($forQueryPlace)'";
            exit();
        }
    }
}